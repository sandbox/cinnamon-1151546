<?php
/*
 * realworks_integration_teaser template
 *
 * Variables:
 * array $object, the object array as provided by realworks_integration_teaser
 * string $ic_preset, the imagecache preset as provided by realworks_integration_teaser
 *
 */
$image = _realworks_integration_get_hoofdfoto($object);

$link = '<span><em>Woningaanbod</em> Bekijk ons complete aanbod</span>';

if ($image):
?>
<div style="background: url('<?php print base_path().imagecache_create_path($ic_preset, $image); ?>') center center no-repeat;">
  <h2>
    <?php print l($link, drupal_get_normal_path('woningaanbod'), array('html'=>true)); ?>
  </h2>
</div>
<?php
endif;

