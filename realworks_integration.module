<?php
/**
 * Realworks synchronisation module
 *
 * @author Jeroen <j.beerstra@cinnamon.nl>
 */

/**
 * Settings
 */
define('REALWORKS_XML_FILE2', file_create_path(file_directory_path() . '/woningaanbod2.xml'));
define('REALWORKS_XSD_FILE2', file_create_path(file_directory_path() . '/woningaanbod2.xsd'));


/**
 * Implementation of hook_cron
 */
function realworks_integration_cron() {
  require('realworks_integration_cron.inc');
  include_once('realworks_integration_get.inc');
  _realworks_integration_sync_data();
}

/**
 * Implementation of hook menu
 * 
 * @return array 
 */
function realworks_integration_menu() {
  $items = array();
  $items['admin/settings/realworks'] = array(
    'title' => 'Realworks',
    'description' => 'Realworks inloggevens instellen.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('realworks_integration_admin'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Realworks admin form
 * 
 * @return array 
 */
function realworks_integration_admin() {
  $form = array();
  $form['realworks_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Realworks webservice uri'),
    '#default_value' => variable_get('realworks_uri', 'http://xml-publish.realworks.nl/servlets/ogexport?koppeling=WEBSITE&user=123456&password=1234&og=WONEN'),
    '#size' => 100,
    '#maxlength' => 140,
    '#description' => t("De complete uri naar de realworks webservice inclusief gebruikersnaam en wachtwoord."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Function to trigger and provide the realworks_integration_overview template
 *
 * @param string $css
 * @param string $ic_preset
 * @param bool $ignore
 * @return array
 */
function realworks_integration_overview($css = '', $ic_preset = 'aanbod-overzicht', $ignore = false) {
  include_once('realworks_integration_get.inc');
  if ($objects = _realworks_integration_get_overview()) {
    $query = _realworks_integration_get_query($ignore);
    $objects = _realworks_integration_process_overview($objects, $query);
    if ($objects) return theme('realworks_integration_overview', $css, $ic_preset, $objects);
  }
  return theme('realworks_integration_overview', $css, $ic_preset, false);
}

/**
 * Function to get a themed teaser for a random object
 *
 * @param string $ic_preset
 * @return string
 */
function realworks_integration_random($ic_preset = 'homepage-teaser') {
  include_once('realworks_integration_get.inc');
  if ($objects = _realworks_integration_get_overview()) {
    $query = _realworks_integration_get_query(true);
    $objects = _realworks_integration_process_overview($objects, $query);
    if (count($objects) > 0) return realworks_integration_teaser($objects[rand(0, count($objects) - 1)], $ic_preset);
  }
  return realworks_integration_teaser(false, $ic_preset);
}

/**
 * Helper function to return the object array for a single object
 *
 * @return mixed
 */
function realworks_integration_fetch_object() {
  include_once('realworks_integration_get.inc');
  if ($objects = _realworks_integration_get_overview()) {
    $query = _realworks_integration_get_query(false);
    $object = _realworks_integration_process_overview($objects, $query);
    if ($object && count($object) == 1) return $object[0];
  }

  return false;
}

/**
 * Function to return themed details for an object (specified by $_GET['id'])
 *
 * @return string
 */
function realworks_integration_details($object) {
  if ($object) return theme('realworks_integration_details', $object);
  
  return theme('realworks_integration_details', false);
}

/**
 * Function to return a themed teaser for an object
 *
 * @param mixed $object
 * @param string $ic_preset
 * @return string
 */
function realworks_integration_teaser($object, $ic_preset) {
  return theme('realworks_integration_teaser', $object, $ic_preset);
}

/**
 * Helper function to trigger and provide the realworks_object_preview template
 * 
 * @param array $object
 * @param string $ic_preset
 * @return array 
 */
function realworks_integration_object_preview($object, $ic_preset = 'aanbod-aside') {
  return theme('realworks_integration_object_preview', $object, $ic_preset);
}

/**
 * Implementation of hook_theme
 * 
 * @return array 
 */
function realworks_integration_theme() {
  return array(
    'realworks_integration_overview' => array(
      'arguments' => array('css' => NULL, 'ic_preset' => NULL, 'objects' => NULL),
      'template' => 'realworks_integration_overview',
    ),
    'realworks_integration_object_preview' => array(
      'arguments' => array('object' => NULL, 'ic_preset' => NULL),
      'template' => 'realworks_integration_object_preview',
    ),
    'realworks_integration_teaser' => array(
      'arguments' => array('object' => NULL, 'ic_preset' => NULL),
      'template' => 'realworks_integration_teaser',
    ),
    'realworks_integration_details' => array(
      'arguments' => array('object' => NULL),
      'template' => 'realworks_integration_details',
    ),
    'realworks_integration_showimages' => array(
      'arguments' => array('object' => NULL, 'images' => NULL, 'ic_presets' => NULL),
      'template' => 'realworks_integration_showimages',
    ),
  );
}

/**
 * Implementation of hook_block().
 *
 * @param string $op
 * @param int $delta
 * @param array $edit
 * @return array
 */
function realworks_integration_block ($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks = array();
      $blocks[0]['title'] = t('Woningaanbod zoeken');
      $blocks[0]['info'] = t('Custom block to search the Realworks objects.');
      $blocks[0]['cache'] = BLOCK_CACHE_GLOBAL;
      return $blocks;
    case 'view':
      include_once('realworks_integration_zoeken.inc');
      $block = array();
      $block['subject'] = t('Woningaanbod zoeken');
      $block['content'] = _realworks_integration_zoeken_block(_realworks_get_xmlobj());
      return $block;
  }
}

/**
 * Function to render the Realworks search form
 * 
 * @return string 
 */
function realworks_integration_zoeken_form() {
  include_once('realworks_integration_zoeken.inc');
  return _realworks_zoeken_block(_realworks_integration_get_xmlobj());
}

/**
 * Helper function to render the price for an object
 *
 * @param array $object
 * @return string
 */
function _realworks_integration_render_price($object) {
  $price = $object['price'];

  $condition = $price['condition'];
  $search = array(
    'kosten koper',
    'vrij op naam',
    'per maand',
    'per jaar',
  );
  $replace = array(
    '<abbr title="Kosten Koper">k.k.</abbr>',
    '<abbr title="Vrij op naam">v.o.n</abbr>',
    '<abbr title="Per maand">p.m.</abbr>',
    '<abbr title="Per jaar">p.j.</abbr>',
  );
  $condition = str_replace($search, $replace, $condition);
  if (!empty($price['price'])) {
    if ($price['type'] == 1 && $price['prefix'] != 'vraagprijs') {
      return ucfirst($price['prefix']).': &euro;&nbsp;' . trim($price['price']) . ' ' . $condition;
    }
    return '&euro;&nbsp;' . trim($price['price']) . ' ' . $condition;
  }
  else {
    return 'Prijs nog niet bekend';
  }
}

/**
 * Helper function to get the 'Hoofdfoto' for a Realworks object
 *
 * @param array $object
 * @return mixed
 */
function _realworks_integration_get_hoofdfoto($object) {
  $query = "SELECT {files}.filepath AS filepath FROM {realworks} LEFT JOIN {files} ON {realworks}.fid = {files}.fid WHERE {realworks}.objectcode = '%s' AND {realworks}.groep = '%s' AND {realworks}.completed = 1 ORDER BY {realworks}.mediaupdate DESC LIMIT 0,1";
  $result = db_query($query, $object['objectcode'], 'HoofdFoto');

  return db_result($result);
}

/**
 * Helper function to get unique values from the realworks Simplexml object from a xpath query
 *
 * @param object $xmlobj
 * @param string $xpath
 * @return array
 */
function _realworks_integration_get_unique($xmlobj, $xpath) {
  $values = array_unique($xmlobj->xpath($xpath));
  sort($values);

  return $values;
}

/**
 * Helper function to load the Realworks xml as a Simplexml object
 *
 * @return mixed
 */
function _realworks_integration_get_xmlobj() {
  if (file_exists(REALWORKS_XML_FILE2) && is_readable(REALWORKS_XML_FILE2) && filesize(REALWORKS_XML_FILE2) > 1024) {
    if (function_exists(simplexml_load_file)) {
      return simplexml_load_file(REALWORKS_XML_FILE2);
    }
  }
  return false;
}

/**
 * Helper function to split object description into intro and rest text
 *
 * @param string $description
 * @return array
 */
function _realworks_integration_format_description($description) {
  $description = str_replace("\n\r", '<br />', $description);
  $description = preg_replace('#(<br\s*?/?>\s*?){1,}#', '</p><p>', $description);
  preg_match('#^([^<]+)</p><p>(.+)$#sD', $description, $matches);

  if (isset($matches[2])) $matches[2] = preg_replace(array('#^(</p><p>){1,}#', '#</p><p>#'), array('',"</p>\n<p>"), $matches[2]);

  return $matches;
}

/**
 * Helper function to get brochures for a Realworks object, if any
 *
 * @param string $objectcode
 * @return array
 */
function _realworks_integration_get_brochures($objectcode) {
  $query = "SELECT {files}.filepath AS filepath, {upload}.list AS list, {upload}.description AS description, {content_type_brochure}.field_adresvereist_value AS adresvereist FROM {node} n LEFT JOIN ({upload} LEFT JOIN {files} ON {files}.fid = {upload}.fid) ON {upload}.nid = n.nid LEFT JOIN {content_type_brochure} ON {content_type_brochure}.nid = n.nid WHERE n.type = 'brochure' and n.status = 1 AND {content_type_brochure}.field_woning_id_value = '%s'";
  $result = db_query($query, $objectcode);
  $brochures = array();
  while ($row = db_fetch_array($result)) {
    $brochures[] = $row;
  }

  return $brochures;
}

/**
 * Helper function to format datetime string
 * 
 * 
 * @param string $string
 * @return string 
 */
function _realworks_integration_format_datetime($string, $format="Y-m-d h:m:s") {
  $return = date($format, strtotime($string));
  $return = preg_replace('%\:0[1-9]\:%', ':00:', $return);

  return $return;
}

/**
 * Helper function to output brochures for a Realworks object, if any
 *
 * @param string $objectcode
 * @return string
 */
function _realworks_integration_render_brochures($objectcode) {
  $brochures = _realworks_integration_get_brochures($objectcode);

  $items = array();
  foreach ($brochures as $data) {
    $ext = substr($data['filepath'], strripos($data['filepath'], '.')+1);
    if ($data['filepath'] != '') {
      if ($data['adresvereist'] == 1 || $data['list']==0)
        $items[] = '<a href="/" rel="' . base64_encode($data['filepath']) . '" title="Download ' . $data['description'] . '" class="adresvereist icon icon-' . $ext . '">' . $data['description'] . '</a>';
      else
        $items[] = '<a href="/' . $data['filepath'] . '" title="Download ' . $data['filename'] . '" class="icon icon-' . $ext . '">' . $data['description'] . '</a>';
    }
  }

  return theme('item_list', $items, NULL, 'ul');
}

/**
 * Helper function to output brochures for a Realworks object, if any
 *
 * @param string $objectcode
 * @return string
 */
function _realworks_integration_render_videos($objectcode) {
	$html = '';
	$result = db_query("SELECT n.nid FROM {node} n WHERE n.type = 'brochure' and n.status = 1");
	$i = 0;
	while ($node = db_fetch_object($result)) {
		$thisNode = node_load($node->nid);
		if ($thisNode->field_woning_id[0]['value'] == $objectcode) {
			if ($thisNode->field_youtube_id[0]['value'] != '') {
				$html .= '<a href="http://www.youtube.com/embed/' . check_plain($thisNode->field_youtube_id[0]['value']) . '?rel=0" class="popover"><span>Bekijk de video</span><img src="http://i1.ytimg.com/vi/' . check_plain($thisNode->field_youtube_id[0]['value']) . '/default.jpg" width="120" height="90" alt="Bekijk de video" /></a>';
				break;
			}
		}
	}
	return $html;
}

/**
 * Function to return the rendered images section for a Realworks object
 *
 * @param mixed $object
 * @param array $ic_presets
 * @return string
 */
function realworks_integration_showimages($object, $ic_presets = array('main' => 'gallery-main', 'others' => 'gallery-thumb', 'lightbox' => 'lightbox')) {
  if ($object) {
    $query = "SELECT {realworks}.rwid AS rwid, {files}.filepath AS filepath FROM {realworks} LEFT JOIN {files} ON {realworks}.fid = {files}.fid WHERE {realworks}.objectcode = '%s' AND {realworks}.groep = '%s' AND {realworks}.completed = 1 ORDER BY {realworks}.mediaupdate DESC LIMIT 0,1";
    $result = db_query($query, $object['objectcode'], 'HoofdFoto');
    
    if ($main = db_fetch_array($result)) {
      $images = array();
      $images['main'] = $main['filepath'];

      $images['others'] = array();
      $query = "SELECT {files}.filepath AS filepath FROM {realworks} LEFT JOIN {files} ON {realworks}.fid = {files}.fid WHERE {realworks}.objectcode = '%s' AND {realworks}.completed = 1 AND {realworks}.rwid != %d ORDER BY {realworks}.rwid";
      $result = db_query($query, $object['objectcode'], $main['rwid']);
      while ($row = db_fetch_array($result)) {
        $images['others'][] = $row['filepath'];
      }

      return theme('realworks_integration_showimages', $object, $images, $ic_presets);
    }      
  }

  return '';
}

/**
 * Function to return the page title for a Realworks object
 *
 * @param mixed $object
 * @return string
 */
function realworks_integration_get_title($object) {
  if ($object) {
    return $object['address']['streetName'] . ' ' . $object['address']['streetNr'] . (!empty($object['address']['streetNrExtension']) ? '-'.$object['address']['streetNrExtension'] : '') . ' ' . $object['address']['city'];
  }

  return 'Woning niet gevonden';
}
