<?php
/**
 * Sync xml + images from realworks
 */
function _realworks_integration_sync_data() {
  $RW_location = null;

  if (file_exists(REALWORKS_XML_FILE2) && is_readable(REALWORKS_XML_FILE2) && filesize(REALWORKS_XML_FILE2) > 1024)
    $RW_location = REALWORKS_XML_FILE2;

  $realworks_lock = variable_get('realworks_lock', 0);
  $realworks_uri = variable_get('realworks_uri', 'http://xml-publish.realworks.nl/servlets/ogexport?koppeling=WEBSITE&user=123456&password=1234&og=WONEN');
  if ($realworks_uri && ((is_null($RW_location)) || (filemtime(REALWORKS_XML_FILE2) < (time() - (24 * 60 * 60)))) && ($realworks_lock < (time() - (24 * 60 * 60)))) {
    variable_set('realworks_lock', time());
    $success = false;

    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,$realworks_uri);
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Drupalrealworks/1.0');
    $RW_data = curl_exec($curl_handle);
    $headers = curl_getinfo($curl_handle);
    curl_close($curl_handle);

    $RW_ok = array(false, false);
    if (is_array($headers)) {
      if ($headers['http_code'] == 200) $RW_ok[0] = true;
      if ($headers['content_type'] == 'application/x-zip;charset=utf-8') $RW_ok[1] = true;
    }

    if ($RW_data && $RW_ok[0] && $RW_ok[1]) {
      $temp_file = tempnam(sys_get_temp_dir(), 'Realworks');
      $fp = fopen($temp_file, "wb");
      fwrite($fp, $RW_data);
      fclose($fp);

      $zip = new ZipArchive;

      $res = $zip->open($temp_file);

      if ($res === true) {
        $crc_error = false;
        for ($i = 0; $i < $zip->numFiles; $i++) {
          $info = $zip->statIndex($i);
          if (preg_match('%\.xml$%', $info['name'])) {
            $xml = _realworks_integration_readcrc($zip, $info, $crc_error);
          }
          elseif (preg_match('%\.xsd$%', $info['name'])) {
            $xsd = _realworks_integration_readcrc($zip, $info, $crc_error);
          }
        }
        $zip->close();

        if (!$crc_error) {
          if (function_exists(simplexml_load_string)) {
            $xml_obj = simplexml_load_string($xml);

            $objects = $xml_obj->xpath('Object');

            $result = _realworks_integration_proces_images($objects);

            if ($result[0]) {
              if ($rv = file_put_contents(REALWORKS_XML_FILE2, $xml, LOCK_EX)) {
                if ($rv = file_put_contents(REALWORKS_XSD_FILE2, $xsd, LOCK_EX)) {
                  variable_del('realworks_lock');
                  $msg = t('Realworks data is gesynchroniseerd.');
                  $success = true;
                }
                else {
                  $msg = t('Fout tijdens wegschrijven Realworks xml metadata.');
                }
              }
              else {
                $msg = t('Fout tijdens wegschrijven Realworks xml data.');
              }
            }
            else {
              $msg = $result[1];
            }
          }
          else {
            $msg = t('Fout tijdens laden vereiste functionaliteit, simplexml module niet aanwezig.');
          }
        }
        else {
          $msg = t('Fout tijdens uitpakken Realworks xml data.');
        }
      }
      else {
        $msg = t('Fout tijdens openen Realworks xml data.');
      }
      unlink($temp_file);
    }
    else {
      $msg = 'Fout tijdens ophalen Realworks xml data.';
    }

    _realworks_integration_report($success, $msg);
  }
}

/**
 * Helper function to get file from zip and check for crc errors
 *
 * @param object $zip
 * @param array $info
 * @param bool $crc_error
 * @return mixed
 */
function _realworks_integration_readcrc($zip, $info, &$crc_error) {
  $fp = $zip->getStream($info['name']);
  ob_end_flush();
  $buf = ""; //file buffer
  ob_start(); //to capture CRC error message
  while (!feof($fp)) {
    $buf .= fread($fp, 2048); //reading more than 2156 bytes seems to disable internal CRC32 verification (bug?)
  }
  $s = ob_get_contents();
  ob_end_clean();
  if(stripos($s, "CRC error") == FALSE){
    return $buf;
  }
  else {
    $crc_error = TRUE;
    return FALSE;
  }
}

/**
 * Helper function to proces images from the Realworks XML
 *
 * @param array $objects
 * @return array
 */
function _realworks_integration_proces_images($objects) {
  $obj_images = array();
  foreach ($objects as $object) {
    $objectcode = (string) $object->ObjectCode;
    $objectdetails = $object->ObjectDetails;
    $address = _realworks_integration_get_address($objectdetails, false);
    $media = $object->xpath('MediaLijst/Media');
    $images = array();
    foreach ($media as $img) {
      $id = (int) $img->Id;
      $group = (string) $img->Groep;
      $mediaupdate = strtotime((string) $img->MediaUpdate);
      $url = (string) $img->URL;
      $images[] = array(
        'id' => $id,
        'group' => $group,
        'mediaupdate' => $mediaupdate,
        'url' => $url,
      );
    }
    if (count($images) > 0) {
      $obj_images[] = array(
        'objectcode' => $objectcode,
        'address' => $address,
        'images' => $images,
      );
    }
  }

  foreach ($obj_images as $images) {
    $result = _realworks_integration_import_images($images);

    if ($result[0] == false) break;
  }
  
  return $result;
}

/**
 * Helper function to import (updated) imafes from the Realworks xml
 *
 * @param array $images
 * @return array
 */
function _realworks_integration_import_images($images) {
  $objectcode = $images['objectcode'];
  $address = $images['address'];
  $return = true;
  $msg = null;

  $query = "SELECT {realworks}.mediaupdate AS mediaupdate, {files}.fid AS fid, {files}.filename AS filename FROM {realworks} LEFT JOIN {files} ON {realworks}.fid = {files}.fid WHERE {realworks}.objectcode = '%s'";
  $result = db_query($query, $objectcode);
  $current = array();
  while ($row = db_fetch_array($result)) {
    $current[$row['filename']] = array(
      'fid' => $row['fid'],
      'mediaupdate' => $row['mediaupdate'],
    );
  }

  $fids = array();
  $updates = array();
  foreach ($images['images'] as $image) {
    $id = $image['id'];
    $group = $image['group'];
    $mediaupdate = $image['mediaupdate'];
    $url = $image['url'];

    $filename = $objectcode.'-'.$group.'-'.$id.'.jpg';
    // 'SEO' fix
    /*$prefix = '';
    if ($address) {
      $prefix .= $address['streetName'];
      $prefix .= '-'.$address['streetNr'];
      if (!empty($address['StreetNrExtentision'])) $prefix .= '-'.$address['StreetNrExtentision'];
      $prefix .= '-'.$address['city'];
      $prefix .= '-client-';
    }
    $filename = $prefix.$objectcode.'-'.$group.'-'.$id.'.jpg';*/

    if (array_key_exists($filename, $current)) {
      $fid = $current[$filename]['fid'];
      $fids[] = $fid;
      if ($mediaupdate > $current[$filename]['mediaupdate']) {
        $updated = true;
        $new = true;
      }
      else {
        $updated = false;
        $new = false;
      }
      $updates[] = $updated;
    }
    else {
      $fid = false;
      $new = true;
    }

    if ($new) {
      if ($img_data = file_get_contents($url, false)) {
        $filepath = file_create_path(file_directory_path(). '/images/realworks/' .$filename);
        if ($filedata = file_put_contents($filepath, $img_data, LOCK_EX)) {
          $file = new stdClass();
          if ($fid) $file->fid = $fid;
          $file->filename = $filename;
          $file->uid = 1;
          $file->status = 1;
          $file->timestamp = time();
          $file->destination = $filepath;
          $file->filepath = $file->destination;
          $file->filemime = 'image/jpeg';
          $file->filesize = filesize($file->filepath);

          drupal_write_record('files',$file);

          if ($fid) {
            $query =  "UPDATE {realworks} SET objectcode = '%s', id = %d, groep = '%s', mediaupdate = %d WHERE fid = %d";
            db_query($query, $objectcode, $id, $group, $mediaupdate, $fid);
          }
          else {
            $query = "INSERT INTO {realworks} (objectcode, id, fid, groep, mediaupdate) VALUES('%s', %d, %d, '%s', %d)";
            db_query($query, $objectcode, $id, $file->fid, $group, $mediaupdate);
            $fids[] = $file->fid;
          }
        } else {
          $return = false;
          $msg = t('Fout tijdens het schrijven van bestand: ') . $filepath;
          break;
        }
      }
      else {
        $return = false;
        $msg = t('Fout tijdens het ophalen van bestand: ') . $url;
        break;
      }
    }
  }

  if ($return) _realworks_integration_cleanup_images($fids, $updates, $objectcode);

  return array($return, $msg);
}

/**
 * Helper function to remove outdated images and database records
 *
 * @param array $fids
 * @param string $objectcode
 */
function _realworks_integration_cleanup_images($fids, $updates, $objectcode) {
  $fids_tmp = implode(',', $fids);

  $old_filenames = array();
  $old_fids = array();
  $query = "SELECT {files}.fid AS fid, {files}.filename AS filename FROM {realworks} LEFT JOIN {files} ON {realworks}.fid = {files}.fid WHERE {realworks}.objectcode = '%s' AND {files}.fid  NOT IN (%s)";
  $result = db_query($query, $objectcode, $fids_tmp);
  while ($row = db_fetch_array($result)) {
    $old_filenames[] = $row['filename'];
    $old_fids[] =  $row['fid'];
  }

  if (count($old_fids) > 0) {
    $old_fids = implode(',', $old_fids);
    $query = "DELETE FROM {realworks} WHERE fid IN (%s)";
    db_query($query, $old_fids);
    $query = "DELETE FROM {files} WHERE fid IN (%s)";
    db_query($query, $old_fids);

    $path = file_create_path(file_directory_path(). '/images/realworks/');
    foreach ($old_filenames as $filename) {
      file_delete($path.$filename);
      imagecache_image_flush($path.$filename);
    }
  }

  $query = "UPDATE {realworks} SET completed = 1 WHERE objectcode = '%s'";
  db_query($query, $objectcode);

  foreach ($updates as $key => $updated) {
    if ($updated) {
      $fid = $fids[$key];
      $query = "SELECT filename FROM {files} WHERE {files}.fid = %d";
      $result = db_query($query, $fid);
      $file = file_create_path(file_directory_path(). '/images/realworks/') . db_result($result);
      imagecache_image_flush($file);
    }
  }
}

/**
 * Helper function to send out mail on error and log a message via watchdog
 *
 * @param bool $success
 * @param string $msg
 */
function _realworks_integration_report($success, $msg) {
  if ($success) {
    watchdog('realworks', $msg, null, WATCHDOG_INFO);
  }
  else {
    $mail = "Er is een probleem opgetreden tijdens het synchroniseren van de xml data van Realworks:\n\n";
    $mail .= $msg;
    $mail .= "\n\nDeze synchronisatie vindt automatisch eenmaal per dag plaats,\n";
    $mail .= "uw huizenbestand zal dan ook op zijn vroegst morgen weer up2date zijn.\n\n";
    $mail .= "Neem eventueel contact op met de Realworks servicedesk.";

    $message = array(
      'id' => 'realworks_mail_report',
      'to' => variable_get('site_mail', 'info@domain.com'),
      'subject' => 'Realworks: probleem met synchronisatie Realworks',
      'body' => drupal_wrap_mail($mail),
      'headers' => array(
        'From' => variable_get('site_mail', 'info@domain.com'),
      ),
    );

    drupal_mail_send($message);
    watchdog('realworks', $msg, null, WATCHDOG_ERROR);
  }
}
