<?php
/*
 * realworks_integration_showimages template
 *
 * Variables:
 * array $object, the realworks_integration object array as provided by realworks_integration_show_images
 * array $images, the images array as provided by realworks_integration_showimages
 * array $ic_presets, the imagecache presets as provided by realworks_integration_showimages
 *
 */
$address = $object['address']['streetName'] . ' ' . $object['address']['streetNr'];
if (!empty($object['address']['StreetNrExtentision'])) $address .= '-' . $object['address']['StreetNrExtentision'];
?>
<div id="woning-thumbs">
    <a href="<?php print base_path().imagecache_create_path($ic_presets['lightbox'], $images['main']); ?>" class="lightbox" rel="gallery" title="<?php print $address . ' Hoofdfoto'; ?>"><?php print theme('imagecache', $ic_presets['main'], $images['main'], $address . ' Hoofdfoto', $address . ' Hoofdfoto'); ?></a>
<?php foreach ($images['others'] as $image): ?>
    <a href="<?php print base_path().imagecache_create_path($ic_presets['lightbox'], $image); ?>" class="lightbox" rel="gallery" title="<?php print $address; ?>"><?php print theme('imagecache', $ic_presets['others'], $image, $address, $address); ?></a>
<?php endforeach; ?>
</div>
