<?php
/**
 * Helper function to get a raw array of Objects from the realworks xmlobj
 * 
 * @return mixed 
 */
function _realworks_integration_get_overview() {
  if ($xml_obj = _realworks_integration_get_xmlobj()) {
    $objects = $xml_obj->xpath('Object');
    if (count($objects) > 0) {
      return $objects;
    }
  }
  return false;
}


/**
 * Helper function to get a processed overview of objects
 *
 * @param object $objects
 * @param mixed $query
 * @return array
 */
function _realworks_integration_process_overview($objects, $query = false) {
  $itemlist = array();
  foreach ($objects as $object) {
    $item = _realworks_integration_get_object($object, $query);
    if ($item) $itemlist[] = $item;
  }

  return _realworks_integration_sort_overview($itemlist);
}

function _realworks_integration_sort_overview($objects) {
  $prices = array();
  foreach ($objects as $k => $v) {
    if (!empty($v['price']['price_int'])) {
      $prices[$k] = $v['price']['price_int'];
    }
    else {
      $prices[$k] = 0;
    }
  }
  array_multisort($prices, SORT_DESC, SORT_NUMERIC, $objects);

  return $objects;
}

/**
 * Helper function to get object details
 *
 * @param object $object
 * @param mixed $query
 * @return mixed
 */
function _realworks_integration_get_object($object, $query) {
  $item = array();
  $objectcode = _realworks_integration_match_object($object, $query);
  if (!$objectcode) return false;
  $item['objectcode'] = $objectcode;
  $objectdetails = $object->ObjectDetails;
  $address = _realworks_integration_get_address($objectdetails, $query);
  if (!$address) return false;
  $item['address'] = $address;
  $price = _realworks_integration_get_price($objectdetails, $query);
  if (!$price) return false;
  $item['price'] = $price;
  $status = _realworks_integration_get_status($objectdetails, array('Ingetrokken'));
  if (!$status) return false;
  $item['status'] = $status;
  // Voorlopig worden alleen woningen en appartementen ondersteund
  $details = _realworks_integration_get_details($object);
  if (!$details) return false;
  $item['details'] = $details;
  $generics = _realworks_integration_get_generics($object, $query);
  if (!$generics) return false;
  $item['generics'] = $generics;
  $levels = _realworks_integration_get_levels($object);
  if (!$levels) return false;
  $item['levels'] = $levels;
  $web = _realworks_integration_get_web($object);
  $item['web'] = $web;

  $item['type'] = _realworks_integration_get_single($objectdetails, 'Bouwvorm', 'string', true);
  $item['description'] = _realworks_integration_get_single($objectdetails, 'Aanbiedingstekst', 'string', true);

  return $item;
}

/**
 * Helper function to match an object against an objectcode if nescessary
 *
 *
 * @param object $object
 * @param mixed $query
 * @return mixed
 */
function _realworks_integration_match_object($object, $query) {
  if ($query && !empty($query['id']) && $query['id'] != (string) $object->ObjectCode) {
    return false;
  }
  return (string) $object->ObjectCode;
}

/**
 * Helper function to get query parameters, if any.
 *
 * @param bool $ignore
 * @return mixed
 */
function _realworks_integration_get_query($ignore) {
  if ($ignore) return false;
  if (isset($_GET['plaats'])) {
    $query = array();
    $query['id'] = null;
    $query['plaats'] = (isset($_GET['plaats']) ? strtolower(check_plain($_GET['plaats'])): 'all');
    $query['type'] = (isset($_GET['type']) ? strtolower(check_plain($_GET['type'])): 'all');
    $query['prijs_van'] = (isset($_GET['prijs_van']) ? abs(($_GET['prijs_van'] == 'all') ? 0 : (int) $_GET['prijs_van']): 0);
    $query['prijs_tot'] = (isset($_GET['prijs_tot']) ? abs(($_GET['prijs_tot'] == 'all') ? 999999999 : (int) $_GET['prijs_tot']): 999999999);
    if ($query['prijs_tot'] < $query['prijs_van']) $query['prijs_tot'] = $query['prijs_van'];
  } elseif (isset($_GET['id']) && !empty($_GET['id'])) {
    $query = array();
    $query['id'] = check_plain($_GET['id']);
    $query['plaats'] = 'all';
    $query['type'] = 'all';
    $query['prijs_van'] = 0;
    $query['prijs_tot'] = 999999999;
  }
  else {
    return false;
  }
  return $query;
}

/**
 * Helper function to get address details for an object
 *
 * @param object $objectdetails
 * @return mixed
 */
function _realworks_integration_get_address($objectdetails, $query) {
  // Voorlopig wordt alleen Nederlands ondersteund
  $addr_obj = $objectdetails->xpath('Adres/Nederlands');
  if (count($addr_obj) == 1) {
    $addr_obj = $addr_obj['0'];
    $address = array();
    $address['country'] = _realworks_integration_get_single($addr_obj, 'Land', 'string', true);
    $address['county'] = _realworks_integration_fixstr(_realworks_integration_get_single($addr_obj, 'Gemeente', 'string'));
    $address['city'] = _realworks_integration_fixstr(_realworks_integration_get_single($addr_obj, 'Woonplaats', 'string', true));
    $address['streetName'] = _realworks_integration_get_single($addr_obj, 'Straatnaam', 'string', true);
    $address['streetNr'] = _realworks_integration_get_single($addr_obj, 'Huisnummer', 'int', true);
    $address['StreetNrExtentision'] = _realworks_integration_get_single($addr_obj, 'HuisnummerToevoeging', 'string');
    $address['zipcode'] = _realworks_integration_get_single($addr_obj, 'Postcode', 'string', true);

    if ($query && $query['plaats'] != 'all') {
      if (strtolower($address['city']) != $query['plaats']) return false;
    }

    return $address;
  }
  else {
    return false;
  }
}

/**
 * Helper function to get price details for an object
 *
 * @param object $objectdetails
 * @return mixed
 */
function _realworks_integration_get_price($objectdetails, $query) {
  $price = array();
  if (isset($objectdetails->Koop)) {
    $koop = $objectdetails->Koop;
    $price['type'] = 1;
    $price['prefix'] = _realworks_integration_get_single($koop, 'Prijsvoorvoegsel', 'string');
    $price['price'] = _realworks_integration_get_single($koop, 'Koopprijs', 'price');
    $price['price_int'] = _realworks_integration_get_single($koop, 'Koopprijs', 'int');
    $price['condition'] = _realworks_integration_get_single($koop, 'KoopConditie', 'string');
    $price['specification'] = _realworks_integration_get_single($koop, 'KoopSpecificatie', 'string');
    $woz = array(
      'value' => array('name' => 'WOZWaarde', 'type' => 'price', 'required' => true),
      'date' => array('name' => 'WOZWaardePeildatum', 'type' => 'string', 'required' => true),
    );
    $price['woz'] = _realworks_integration_get_subsection($koop, 'WOZ', $woz);
  }
  elseif (isset($objectdetails->Huur)) {
    $huur = $objectdetails->Huur;
    $price['type'] = 2;
    $price['price'] = _realworks_integration_get_single($huur, 'Huurprijs', 'price', true);
    $price['price_int'] = _realworks_integration_get_single($huur, 'Huurprijs', 'int', true);
    $price['condition'] = _realworks_integration_get_single($huur, 'HuurConditie', 'string', true);
    $price['specification'] = _realworks_integration_get_single($huur, 'HuurSpecificatie', 'string');
  }
  else {
    return false;
  }

  $price['servicecosts'] = _realworks_integration_get_single($objectdetails, 'ServicekostenPerMaand', 'price');
  $price['accept'] = _realworks_integration_get_single($objectdetails, 'Aanvaarding', 'string', true);
  $price['acceptdate'] = _realworks_integration_get_single($objectdetails, 'DatumAanvaarding', 'string');
  $price['accepttext'] = _realworks_integration_get_single($objectdetails, 'ToelichtingAanvaarding', 'string');
  $price['application'] = _realworks_integration_get_single($objectdetails, 'ObjectAanmelding', 'string', true);

  $combination = array(
    'type' => array('name' => 'Mengvorm', 'type' => 'string', 'required' => true),
    'difference' => array('name' => 'AfwijkingVanKoopprijs', 'type' => 'price', 'required' => false),
    'explanation' => array('name' => 'Toelichting', 'type' => 'string', 'required' => false),
  );
  $price['combination'] = _realworks_integration_get_subsection($objectdetails, 'Koopmengvorm', $combination);

  if ($query) {
    $price_number = (int) preg_replace("#[^0-9]#","",$price['price']);
    if ($price_number <= $query['prijs_van']) return false;
    if ($price_number >= $query['prijs_tot']) return false;
  }

  return $price;
}

/**
 * Helper function to get status details for an object
 *
 * @param object $objectdetails
 * @return array
 */
function _realworks_integration_get_status($objectdetails, $hide) {
  $status = array();
  $status_obj = $objectdetails->StatusBeschikbaarheid;
  $status['status'] = _realworks_integration_fixstr(_realworks_integration_get_single($status_obj, 'Status', 'string', true));
  if (in_array($status['status'], $hide)) return false;
  $reservation = array(
    'date' => array('name' => 'Datum', 'type' => 'string', 'required' => true),
    'expires' => array('name' => 'DatumVoorbehoudTot', 'type' => 'string', 'required' => false),
  );
  $status['reservation'] = _realworks_integration_get_subsection($status_obj, 'VerkochtOnderVoorbehoud', $reservation);
  $status['transactiondate'] = _realworks_integration_get_single($status_obj, 'TransactieDatum', 'string');

  return $status;
}

/**
 * Helper function to get other details for an object
 *
 * @param object $object
 * @return mixed
 */
function _realworks_integration_get_details($object) {
  if (isset($object->Wonen)) {
    $wonen = array();
    $wonendetails = $object->Wonen->WonenDetails;

    $designation = array(
      'Huidig Gebruik' => array('name' => 'HuidigGebruik', 'type' => 'string', 'required' => false),
      'Huidige Bestemming' => array('name' => 'HuidigeBestemming', 'type' => 'string', 'required' => false),
      'Permanente Bewoning' => array('name' => 'PermanenteBewoning', 'type' => 'string', 'required' => true),
      'Recreatiewoning' => array('name' => 'Recreatiewoning', 'type' => 'string', 'required' => true),
    );
    $wonen['Bestemming'] = _realworks_integration_get_subsection($wonendetails, 'Bestemming', $designation, true);

    $measurements= array(
      'Liggingen' => array('name' => 'Liggingen', 'type' => 'subsection', 'required' => false, 'subs' => array('Ligging' => array('name' => 'Ligging', 'type' => 'multiple', 'required' => false,  'mtype' => 'string'))),
      'Inhoud' => array('name' => 'Inhoud', 'type' => 'int', 'required' => true),
      'Gebruiksoppervlakte Woonfunctie' => array('name' => 'GebruiksoppervlakteWoonfunctie', 'type' => 'int', 'required' => true),
      'Gebruiksoppervlakte Overige Functies' => array('name' => 'GebruiksoppervlakteOverigeFuncties', 'type' => 'int', 'required' => false),
      'Buitenruimtes Gebouwgebonden Of Vrijstaand' => array('name' => 'BuitenruimtesGebouwgebondenOfVrijstaand', 'type' => 'int', 'required' => false),
      'Perceel Oppervlakte' => array('name' => 'PerceelOppervlakte', 'type' => 'int', 'required' => false),
    );
    $wonen['Maten En Ligging'] = _realworks_integration_get_subsection($wonendetails, 'MatenEnLigging', $measurements, true);

    $build = array(
      'Jaar Omschrijving' => array('name' => 'JaarOmschrijving', 'type' => 'subsection', 'required' => false, 'subs' => array(
        'Jaar' => array('name' => 'Jaar', 'type' => 'string', 'required' => true),
        'Omschrijving' => array('name' => 'Omschrijving', 'type' => 'string', 'required' => false),
      ) ,'multiple' => true),
      'Periode' => array('name' => 'Periode', 'type' => 'string', 'required' => false),
      'In Aanbouw' => array('name' => 'InAanbouw', 'type' => 'string', 'required' => true),
    );
    $wonen['Bouwjaar'] = _realworks_integration_get_subsection($wonendetails, 'Bouwjaar', $build, true);


    $maintenance = array(
      'Binnen' => array('name' => 'Binnen', 'type' => 'subsection', 'required' => true, 'subs' => array(
        'Waardering' => array('name' => 'Waardering', 'type' => 'string', 'required' => true),
        'Memo' => array('name' => 'Memo', 'type' => 'string', 'required' => false),
      )),
      'Buiten' => array('name' => 'Buiten', 'type' => 'subsection', 'required' => true, 'subs' => array(
        'Waardering' => array('name' => 'Waardering', 'type' => 'string', 'required' => true),
        'Memo' => array('name' => 'Memo', 'type' => 'string', 'required' => false),
      )),
    );
    $wonen['Onderhoud'] = _realworks_integration_get_subsection($wonendetails, 'Onderhoud', $maintenance, true);

    $storage = array(
      'Soort' => array('name' => 'Soort', 'type' => 'string', 'required' => true),
      'Voorzieningen' => array('name' => 'Voorzieningen', 'type' => 'string', 'required' => false),
      'Isolatievormen' => array('name' => 'Isolatievormen', 'type' => 'insulation', 'required' => false),
      'Totaal Aantal' => array('name' => 'TotaalAantal', 'type' => 'string', 'required' => true),
    );
    $wonen['Schuur Berging'] = _realworks_integration_get_subsection($wonendetails, 'SchuurBerging',  $storage);

    $various = array(
      'Bijzonderheden' => array('type' => 'subsection', 'name' => 'Bijzonderheden', 'required' => false, 'subs' => array('special' => array('type' => 'multiple', 'name' => 'Bijzonderheid', 'required' => true, 'mtype' => 'string'))),
      'Isolatievormen' => array('name' => 'Isolatievormen', 'type' => 'insulation', 'required' => false),
      'Dak' => array('name' => 'Dak', 'type' => 'subsection', 'required' => false, 'subs' => array(
        'Type' => array('name' => 'Type', 'type' => 'string', 'required' => false),
        'Materialen' => array('name' => 'Materialen', 'type' => 'multiple', 'required' => false, 'mtype' => 'string'),
        'Toelichting' => array('name' => 'Toelichting', 'type' => 'string', 'required' => false),
      )),
    );
    $wonen['Diversen'] = _realworks_integration_get_subsection($wonendetails, 'Diversen', $various);

    $wonen['Keurmerken'] = _realworks_integration_get_subsection($wonendetails, 'Keurmerken', array('award' => array('name' => 'Keurmerk', 'type' => 'multiple', 'required' => true, 'mtype' => 'string')), false);

    $energylabel = array(
      'Energieklasse' => array('name' => 'Energieklasse', 'type' => 'string', 'required' => true),
      'Energie Index' => array('name' => 'EnergieIndex', 'type' => 'string', 'required' => false),
      'Einddatum' => array('name' => 'Einddatum', 'type' => 'string', 'required' => false),
    );
    $wonen['Energielabel'] = _realworks_integration_get_subsection($wonendetails, 'Energielabel', $energylabel);

    $wonen['EPC'] = _realworks_integration_get_single($wonendetails, 'EPC', 'string', false);

    $installation = array(
      'Soorten Verwarming' => array('name' => 'SoortenVerwarming', 'type' => 'subsection', 'required' => false, 'subs' => array('Verwarming' => array('name' => 'Verwarming', 'type' => 'multiple', 'required' => true, 'mtype' => 'string'))),
      'CV Ketel' => array('name' => 'CVKetel', 'type' => 'subsection', 'required' => false, 'subs' => array(
        'CV Ketel Type' => array('name' => 'CVKetelType', 'type' => 'string', 'required' => false),
        'Bouwjaar' => array('name' => 'Bouwjaar', 'type' => 'string', 'required' => false),
        'Brandstof' => array('name' => 'Brandstof', 'type' => 'string', 'required' => false),
        'Eigendom' => array('name' => 'Eigendom', 'type' => 'string', 'required' => false),
        'Combiketel' => array('name' => 'Combiketel', 'type' => 'string', 'required' => false),
      ))
    );
    $wonen['Installatie'] = _realworks_integration_get_subsection($wonendetails, 'Installatie', $installation);

    $wonen['Soorten Warm Water'] = _realworks_integration_get_subsection($wonendetails, 'SoortenWarmWater', array('type' => array('name' => 'WarmWater', 'type' => 'multiple', 'required' => true, 'mtype' => 'string')));

    $wonen['Voorzieningen Wonen'] = _realworks_integration_get_subsection($wonendetails, 'VoorzieningenWonen', array('extra' => array('name' => 'Voorziening', 'type' => 'multiple', 'required' => true, 'mtype' => 'string')));

    $wonen['Toelichting'] = _realworks_integration_get_single($wonendetails, 'Toelichting', 'string');

    $garden = array(
      'Tuintypen' => array('name' => 'Tuintypen', 'type' => 'subsection', 'required' => true, 'subs' => array('Tuintype' => array('name' => 'Tuintype', 'type' => 'multiple', 'required' => true, 'mtype' => 'string'))),
      'Kwaliteit' => array('name' => 'Kwaliteit', 'type' => 'string', 'required' => false),
      'Totale Oppervlakte' => array('name' => 'TotaleOppervlakte', 'type' => 'int', 'required' => false),
    );
    $wonen['Tuin'] = _realworks_integration_get_subsection($wonendetails, 'Tuin', $garden, true);

    $maingarden = array(
      'Type' => array('name' => 'Type', 'type' => 'string', 'required' => true),
      'Positie' => array('name' => 'Positie', 'type' => 'string', 'required' => false),
      'Achterom' => array('name' => 'Achterom', 'type' => 'string', 'required' => false),
      'Afmetingen' => array('name' => 'Afmetingen', 'type' => 'measurements', 'required' => false),
    );
    $wonen['Hoofdtuin'] = _realworks_integration_get_subsection($wonendetails, 'Hoofdtuin', $maingarden);

    $garage = array(
      'Soorten' => array('name' => 'Soorten', 'type' => 'subsection', 'required' => true, 'subs' => array('Soort' => array('name' => 'Soort', 'type' => 'multiple', 'required' => true, 'mtype' => 'string'))),
      'Voorzieningen' => array('name' => 'Voorzieningen', 'type' => 'subsection' , 'required' => false, 'subs' => array('Voorziening' => array('name' => 'Voorziening', 'type' => 'multiple', 'required' => true, 'mtype' =>  'string'))),
      'Capaciteit' => array('name' => 'Capaciteit', 'type' => 'int', 'required' => false),
      'Afmetingen' => array('name' => 'Afmetingen', 'type' => 'measurements', 'required' => false),
      'Isolatievormen' => array('name' => 'Isolatievormen', 'type' => 'insulation', 'required' => false),
      'Totaal Aantal Garages' => array('name' => 'TotaalAantalGarages', 'type' => 'int', 'required' =>  false),
    );
    $wonen['Garage'] = _realworks_integration_get_subsection($wonendetails, 'Garage', $garage, true);

    $parking = array(
      'Facaliteiten' => array('name' => 'Facaliteiten', 'type' => 'subsection', 'required' => false, 'subs' => array('property' => array('name' => 'Facaliteit', 'type' => 'multiple', 'required' => true, 'mtype' => 'stirng'))),
      'Toelichting' => array('name' => 'Toelichting', 'type' => 'string', 'required' => false),
    );
    $wonen['Parkeren'] = _realworks_integration_get_subsection($wonendetails, 'Parkeren', $parking);

    return $wonen;
  }
  else {
    return false;
  }
}

/**
 * Helper function to get generic properties for an object
 *
 * @param object $object
 * @return mixed
 */
function _realworks_integration_get_generics($object, $query) {
  if (isset($object->Wonen)) {
    $generics = array();
    if (isset($object->Wonen->Woonhuis)) {
      $house = $object->Wonen->Woonhuis;
      $generics['type'] = 'woonhuis';
      $generics['sort'] = _realworks_integration_get_single($house, 'SoortWoning', 'string', true);
      $generics['typewoning'] = _realworks_integration_get_single($house, 'TypeWoning', 'string', true);
      $generics['specifics'] = _realworks_integration_get_single($house, 'KenmerkWoning', 'string');
      $generics['quality'] = _realworks_integration_get_single($house, 'KwaliteitWoning', 'string');
    }
    else {
      $appartment = $object->Wonen->Appartement;
      $generics['type'] = 'appartement';
      $generics['sort'] = _realworks_integration_get_single($appartment, 'SoortAppartement', 'string', true);
      $generics['specifics'] = _realworks_integration_get_single($appartment, 'KenmerkAppartement', 'string');
      $generics['quality'] = _realworks_integration_get_single($appartment, 'KwaliteitAppartement', 'string');
      $generics['openportiek'] = _realworks_integration_get_single($appartment, 'OpenPortiek', 'string', true);
      $generics['layer'] = _realworks_integration_get_single($appartment, 'Woonlaag', 'int');
      $generics['layers'] = _realworks_integration_get_single($appartment, 'AantalWoonlagen', 'int', true);
      $generics['checklist'] = _realworks_integration_get_single($appartment, 'VVEChecklistAanwezig', 'int');
    }

    if ($query && $query['type'] != 'all') {
      if (strtolower($generics['sort']) != $query['type']) return false;
    }

    return $generics;
  }
  else {
    return false;
  }
}

/**
 * Helper function to get room details for an object
 *
 * @param object $object
 * @return mixed
 */

function _realworks_integration_get_levels($object) {
  if (isset($object->Wonen)) {
    $levels = array();
    $levelobj = $object->Wonen->Verdiepingen;
    $levels['Aantal'] = _realworks_integration_get_single($levelobj, 'Aantal', 'int', true);
    $levels['Aantal Kamers'] = _realworks_integration_get_single($levelobj, 'AantalKamers', 'int', true);
    $levels['Aantal Slaapkamers'] = _realworks_integration_get_single($levelobj, 'AantalSlaapKamers', 'int', true);

    return $levels;
  }
  else {
    return false;
  }
}

/**
 * Helper function to get web details for an object, if any
 *
 * @param object $object
 * @return array
 */
function _realworks_integration_get_web($object) {
  $web = array();
  if (isset($object->Web)) {
    $webobj = $object->Web;
    $web['Prijs Tonen'] = _realworks_integration_get_single($webobj, 'PrijsTonen', 'string');
    $openhuis = array(
      'Vanaf' => array('name' => 'Vanaf', 'type' => 'string', 'required' => true),
      'Tot' => array('name' => 'Tot', 'type' => 'string', 'required' => true),
    );
    $web['Open Huis'] = _realworks_integration_get_subsection($webobj, 'OpenHuis', $openhuis);
    $web['Prioriteit'] = _realworks_integration_get_single($webobj, 'Prioriteit', 'string');
    $web['Magazine'] = _realworks_integration_get_single($webobj, 'Magazine', 'string');
  }

  return $web;
}

/**
 * Helper function to get insulation details, basicly a special case of _realworks_integration_get_multiple
 *
 * @param object $object
 * @param string $path
 * @return array
 */
function _realworks_integration_get_insulation($object, $path) {
  $result = array();
  $path = _realworks_integration_flatten($path);
  $ins_objs = $object->xpath('/' . $path . '/Isolatie');
  foreach ($ins_objs as $ins_obj) {
    $result[] = (string) $ins_obj;
  }
  return $result;
}

/**
 * Helper function to get measurement details, a special case of _realworks_integration_get_subsection
 *
 * @param object $object
 * @param string $path
 * @param bool $required
 * @return array
 */
function _realworks_integration_get_measurements($object, $path, $required = false) {
  $subs = array(
    'Lengte' => array('name' => 'Lengte', 'type' => 'int', 'required' => false),
    'Breedte' => array('name' => 'Breedte', 'type' => 'int', 'required' => false),
    'Oppervlakte' => array('name' => 'Oppervlakte', 'type' => 'int', 'required' => false),
  );

  return _realworks_integration_get_subsection($object, $path, $subs, $required);
}

/**
 * Helper function to get multiple results for a simplexml element
 *
 * @param object $object
 * @param string $path
 * @param string $type
 * @return array
 */
function _realworks_integration_get_multiple($object, $path, $type) {
  $result = array();
  $path = _realworks_integration_flatten($path);
  $objs = $object->xpath($path);
  foreach ($objs as $obj) {
    $result[] = _realworks_integration_typecast($obj, $type);
  }
  return $result;
}

/**
 * Helper function to get a single result for a simplexml element
 *
 * @param object $object
 * @param string $path
 * @param string $type
 * @param bool $required
 * @return mixed
 */
function _realworks_integration_get_single($object, $path, $type, $required = false) {
  if (!$required) {
    if (isset($object->$path)) {
      return _realworks_integration_typecast($object->$path, $type);    
    }
    else {
      return '';
    }
  }
  else {
    return _realworks_integration_typecast($object->$path, $type);
  }
}

/**
 * Helper function to typecast a simplexml element
 *
 * @param object $obj
 * @param string $type
 * @return mixed
 */
function _realworks_integration_typecast($obj, $type = 'string') {
  switch($type) {
    case 'string':
      return (string) $obj;
    case 'int':
      return (int) $obj;
    case 'float':
      return (float) $obj;
    case 'price':
      return number_format((float) $obj, 0, ',', '.') . ',&ndash;';
  }
}

/**
 * Helper function to transform a specified Realworks subsection into an array
 *
 * @param object $object
 * @param string $path
 * @param array $subs
 * @param bool $required
 * @param bool $multiple
 * @return array
 */
function _realworks_integration_get_subsection($object, $path, $subs, $required = false, $multiple = false) {
  if (!$required) {
    if (isset($object->$path)) {
      return _realworks_integration_get_subsection_execute($object, $path, $subs, $multiple);
    }
    else {
      return array();
    }
  }
  else {
    return _realworks_integration_get_subsection_execute($object, $path, $subs, $muliple);
  }
}

/**
 * Execute _realworks_integration_get_subsection selection
 *
 * @param object $object
 * @param string $path
 * @param array $subs
 * @return array
 */
function _realworks_integration_get_subsection_execute($object, $path, $subs, $multiple) {
  if ($multiple) {
    $result = array();
    $query = _realworks_integration_flatten($path);
    $objs = $object->xpath($query);
    foreach ($objs as $obj) {
      $subresult = _realworks_integration_process_subs($obj, null, $subs);
      $result[] = $subresult;
    }
  }
  else {
    $result = _realworks_integration_process_subs($object, $path, $subs);
  }

  return $result;
}

/**
 * Helper function for _realworks_integration_get_subsection_execute
 *
 * @param object $object
 * @param string $path
 * @param array $subs
 * @return array
 */
function _realworks_integration_process_subs($object, $path, $subs) {
  $result = array();
  foreach ($subs as $key => $properties) {
    $result[$key] = _realworks_integration_proces_sub($properties, $object, $path);
  }

  return $result;
}

/**
 * Helper function for _realworks_integration_process_subs, process a subsection element (can be a subsection itself)
 *
 * @param array $properties
 * @param object $object
 * @param string $path
 * @return mixed
 */
function _realworks_integration_proces_sub($properties, $object, $path) {
  $type = $properties['type'];
  $name = $properties['name'];
  $required = $properties['required'];
  !empty($path) ? $object = $object->$path: $object;
  switch ($type) {
    case 'multiple':
      $type = $properties['mtype'];
      return _realworks_integration_get_multiple($object, $name, $type);
    case 'subsection':
      $multiple = $properties['multiple'];
      $subs = $properties['subs'];
      return _realworks_integration_get_subsection($object, $name, $subs, $required, $multiple);
    case 'insulation':
      return _realworks_integration_get_insulation($object, $name);
    case 'measurements':
      return _realworks_integration_get_measurements($object, $name, $required);
    default:
      return _realworks_integration_get_single($object, $name, $type, $required);
  }
}

/**
 * Transform a simplexml element path to a xpath selector
 *
 * @param string $path
 * @return string
 */
function _realworks_integration_flatten($path) {
  return str_replace('->', '/', $path);
}

/**
 * Helper function to fix realworks strings
 *
 * @param str $str
 * @return str
 */

function _realworks_integration_fixstr($str) {
  return ucfirst(strtolower($str));
}
