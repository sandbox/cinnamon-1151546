<?php
/* 
 * realworks_integration_overview template
 *
 * Variables:
 * array $objects, the objects array (false if empty)
 * string $css, specified classes if any (via realworks_integration_overview($css, $ic_preset))
 * string $ic_preset, imagecache preset if any (via realworks_integration_overview($css, $ic_preset))
 *
 */
if ($objects):
?>
<ul<?php if (!empty($css)) print ' class="'.$css.'"';?>>
<?php foreach ($objects as $object) print realworks_integration_object_preview($object, $ic_preset); ?>
</ul>
<?php
else:
?>
<p>Geen woningen gevonden.</p>
<?php
endif;


