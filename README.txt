This is an initial module to integrate Realworks synchronisation into Drupal, xml and images are synchronised once a day, only new or updated images are imported into Drupal.

There are several templates and associated functions you can use in your template to generate the real estate output:

realworks_overview()
realworks_teaser()
realworks_details()
realworks_showimages()

There is also a search block you can use to search the available real estate.

This module requires PHP's SimpleXML

At the moment the module is closely tied with our clients requirements, not all data is processed for example only a fixed set of fields. You can always contact us for customization or help: info@cinnamon.nl
