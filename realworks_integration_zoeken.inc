<?php
/**
 * Include for the Woningaanbod zoeken block
 *
 */

/**
 * Helper function to render the Realworks search form
 *
 * @param object $xmlobj
 */
function _realworks_integration_zoeken_block($xmlobj) {
?>
				<form action="/woningaanbod" method="get" class="aanbod-zoeken">
					<fieldset>
						<legend>Zoek in woningaanbod</legend>
						<div class="formrow">
							<div class="label">
								<label for="plaats">Plaats</label>
							</div>
							<div class="field">
								<select name="plaats" id="plaats">
									<option value="all">- Alle -</option>
<?php
  foreach (_realworks_integration_get_unique($xmlobj, '//Woonplaats') as $value) {
    $value = ucfirst(strtolower($value));
?>
									<option value="<?php echo $value; ?>"<?php if (isset($_GET['plaats']) && $_GET['plaats']==$value) print ' selected="selected"'; ?>><?php echo $value; ?></option>
<?php
  }
?>
								</select>
							</div>
						</div>
						<div class="formrow">
							<div class="label">
								<label for="type">Type</label>
							</div>
							<div class="field">
								<select name="type" id="type">
									<option value="all">- Alle -</option>
<?php
  $woning_types = _realworks_integration_get_unique($xmlobj, '//SoortWoning');
  $appartement_types = _realworks_integration_get_unique($xmlobj, '//SoortAppartement');
  $types = array_unique(array_merge($woning_types, $appartement_types));
  sort($types);

  foreach ($types as $value) {
    $value = ucfirst(strtolower($value));
?>
									<option value="<?php echo $value; ?>"<?php if (isset($_GET['type']) && $_GET['type']==$value) print ' selected="selected"'; ?>><?php echo $value; ?></option>
<?php
  }
?>
								</select>
							</div>
						</div>
						<div class="formrow">
							<div class="label">
								<label for="prijs">Prijs</label>
							</div>
							<div class="field">
								<select name="prijs_van" id="prijs_van" class="half">
									<option value="all">&euro; 0</option>
<?php
  $prices = array(50000, 75000, 100000, 125000, 150000, 175000, 200000, 225000, 250000, 275000, 300000, 325000, 350000, 375000, 400000, 450000, 500000, 550000, 600000, 650000, 700000, 750000, 800000, 900000, 1000000, 1250000, 1500000, 2000000);
  foreach ($prices as $price) {
    print '<option value="' . $price . '"';
    if (isset($_GET['prijs_van']) && $_GET['prijs_van']==$price) print ' selected="selected"';
    print '>&euro; ' . $price . '</option>';
  }
?>
								</select>
								t/m
								<select name="prijs_tot" id="prijs_tot" class="half">
									<option value="all">Geen maximum</option>
<?php
  foreach ($prices as $price) {
    print '<option value="' . $price . '"';
    if (isset($_GET['prijs_tot']) && $_GET['prijs_tot']==$price) print ' selected="selected"';
    print '>&euro; ' . $price . '</option>';
  }
?>
								</select>
							</div>
						</div>
						<button type="submit">Zoek</button>
					</fieldset>
				</form>
<?php
}
