<?php
/*
 * realworks_integration_object_preview template
 *
 * Variables:
 * array $object, the object array as provided by realworks_integration_overview
 * string $ic_preset, the imagecache preset as provided by realworks_integration_overview
 *
 */
$image = _realworks_integration_get_hoofdfoto($object);

$address = $object['address']['streetName'] . ' ' . $object['address']['streetNr'];
if (!empty($object['address']['StreetNrExtentision'])) $address .= '-' . $object['address']['StreetNrExtentision'];

if ($object['status']['status'] != 'Beschikbaar') $status = '<span class="status">'.$object['status']['status'].'</span>';
//if (count($object['status']['reservation']) > 0) $status .= ' <span class="st-reservation">(Verkocht onder voorbehoud '.$object['status']['reservation']['date'].', geldig tot '.$object['status']['reservation']['expires'].')</span>';
//if (!empty($object['status']['transactiondate'])) $status .= ' <span class="st-transdate">(Transactiedatum '.$object['status']['transactiondate'].')</span>';

?>
<li>
  <a href="/woning?id=<?php print $object['objectcode']; ?>">
     <?php if ($image) print theme('imagecache', $ic_preset, $image, $address . ' Hoofdfoto', $address . ' Hoofdfoto'); ?>
     <strong><?php print $object['address']['city']; ?></strong><br /><span class="adr-straat"><?php print $address; ?></span><br />
     <?php print _realworks_integration_render_price($object); ?>
     <?php print $status ?>
  </a>
</li>
