<?php
/*
 * realworks_integration_details template
 *
 * Variables:
 * array $object, the object array as provided by realworks_integration_details
 *
 */
if ($object) {
  $address = $object['address']['streetName'] . ' ' . $object['address']['streetNr'];
  if (!empty($object['address']['StreetNrExtentision'])) $address .= '-' . $object['address']['StreetNrExtentision'];

  $status = $object['status']['status'];
  if (count($object['status']['reservation']) > 0) $status .= ' <span class="st-reservation">'.$object['status']['reservation']['date'].(!empty($object['status']['reservation']['expires']) ? ', geldig tot '.$object['status']['reservation']['expires']: '').'</span>';
  if (!empty($object['status']['transactiondate'])) $status .= ' <span class="st-transdate">Transactiedatum '.$object['status']['transactiondate'].')</span>';

  $brochures = _realworks_integration_render_brochures($object['objectcode']);
  $videos = _realworks_integration_render_videos($object['objectcode']);
  $description = _realworks_integration_format_description($object['description']);
?>
<div id="content-header">
	<h1><?php print $address; ?></h1>
</div>
<div id="content-area">
	<div class="metadata mediumTxt">
		<p class="price"><?php print _realworks_integration_render_price($object); ?></p>
		<?php if (!empty($status) && $status != 'Beschikbaar') { ?><p class="status">(<?php print $status; ?>)</p><?php } ?>
		<p class="address" title="<?php print $address . ',' . _realworks_integration_fixstr($object['address']['city']) . ',' . substr($object['address']['zipcode'], 0, 4); ?>,NL">
			<?php print $object['address']['zipcode']; ?>
			<span class="adr-plaats"><?php print _realworks_integration_fixstr($object['address']['city']); ?></span>
		</p>
		<?php if (strstr($brochures, '<ul')) { print '<h2>Brochures</h2>' . $brochures; } ?>
		<?php if (!empty($videos)) { print '<h2>Video</h2><p class="video">' . $videos . '</p>'; } ?>
	</div>
	<p class="intro"><?php print $description[1]; ?></p>
	<p><?php print $description[2]; ?></p>
<?php //Todo print _realworks_integration_render_table('GEGEVENS VAN DEZE WONING', $object['details']); ?>
  <table summary="">
    <caption>Gegevens van deze woning</caption>
    <tbody>
<?php if ((count($object['details']['Bouwjaar']['Jaar Omschrijving']) > 0) && !empty($object['details']['Bouwjaar']['Jaar Omschrijving'][0]['Jaar'])): ?>
      <tr>
          <th scope="row">Bouwjaar</th>
          <td><?php print $object['details']['Bouwjaar']['Jaar Omschrijving'][0]['Jaar']; ?></td>
      </tr>
<?php elseif (!empty($object['details']['Bouwjaar']['Periode'])): ?>
      <tr>
          <th scope="row">Periode</th>
          <td><?php print $object['details']['Bouwjaar']['Periode']; ?></td>
      </tr>
<?php endif; ?>
<?php if (!empty($object['details']['Maten En Ligging']['Perceel Oppervlakte'])): ?>
      <tr>
          <th scope="row">Perceeloppervlakte</th>
          <td><?php print $object['details']['Maten En Ligging']['Perceel Oppervlakte']; ?> m2</td>
      </tr>
<?php endif; ?>
      <tr>
          <th scope="row">Woonoppervlakte</th>
          <td><?php print $object['details']['Maten En Ligging']['Gebruiksoppervlakte Woonfunctie']; ?> m2</td>
      </tr>
      <tr>
          <th scope="row">Inhoud</th>
          <td><?php print $object['details']['Maten En Ligging']['Inhoud']; ?> m3</td>
      </tr>
      <tr>
          <th scope="row">Aantal kamers</th>
          <td><?php print $object['levels']['Aantal Kamers']; ?></td>
      </tr>
      <tr>
          <th scope="row">Aantal slaapkamers</th>
          <td><?php print $object['levels']['Aantal Slaapkamers']; ?></td>
      </tr>
      <tr>
	<th scope="row">Soort woonhuis</th>
        <td><?php print ucfirst($object['generics']['sort']); ?></td>
      </tr>
<?php if (isset($object['generics']['typewoning']) && !empty($object['generics']['typewoning'])): ?>
      <tr>
          <th scope="row">Subtype</th>
          <td><?php print ucfirst($object['generics']['typewoning']); ?></td>
      </tr>
<?php endif; ?>
      <tr>
          <th scope="row">Tuin</th>
          <td><?php print (isset($object['details']['Tuin']['Tuintypen']['Tuintype'][0]) ? ucfirst($object['details']['Tuin']['Tuintypen']['Tuintype'][0]): '&ndash;'); ?></td>
      </tr>
      <tr>
          <th scope="row">Garage</th>
          <td><?php print (isset($object['details']['Garage']['Soorten']['Soort'][0]) ? ucfirst($object['details']['Garage']['Soorten']['Soort'][0]): '&ndash;'); ?></td>
      </tr>
<?php if (count($object['details']['Schuur Berging']) > 0): ?>
      <tr>
          <th scope="row">Schuur</th>
          <td><?php print ucfirst($object['details']['Schuur Berging']['Soort']); ?></td>
      </tr>
<?php endif; ?>
<?php if ((count($object['details']['Maten En Ligging']['Liggingen']) > 0) && (count($object['details']['Maten En Ligging']['Liggingen']['Ligging']) > 0)): ?>
      <tr>
          <th scope="row">Locatie</th>
          <td><?php print ucfirst($object['details']['Maten En Ligging']['Liggingen']['Ligging'][0]); ?></td>
      </tr>
<?php endif; ?>
<?php if ((count($object['details']['Installatie']) > 0) && (count($object['details']['Installatie']['Soorten Verwarming']) > 0) && (count($object['details']['Installatie']['Soorten Verwarming']['Verwarming']) > 0)): ?>
      <tr>
          <th scope="row">Verwarming</th>
          <td><?php print ucfirst($object['details']['Installatie']['Soorten Verwarming']['Verwarming'][0]); ?></td>
      </tr>
<?php endif; ?>
      <tr>
          <th scope="row">Open huis</th>
<?php if ((count($object['web']) > 0) && (count($object['web']['Open Huis']) > 0)): ?>
          <td>Ja (vanaf: <?php print _realworks_integration_format_datetime($object['web']['Open Huis']['Vanaf']); ?> tot: <?php print _realworks_integration_format_datetime($object['web']['Open Huis']['Tot']); ?>)</td>
<?php else: ?>
          <td>Nee</td>
<?php endif; ?>
      </tr>
    </tbody>
  </table>
</div>
<?php
}
else {
?>
<p>Woning niet gevonden.</p>
<?php
}
